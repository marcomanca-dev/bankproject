package it.si2001.bankproject.service;

import it.si2001.bankproject.domain.Balance;
import it.si2001.bankproject.domain.dto.Response;

/**
 * Service Interface for managing {@link Balance}.
 */
public interface BalanceService {

    /**
     * Get the "id" report.
     *
     * @param id the id of the account.
     * @return the response entity.
     */
    Response getAccountBalance(Long id);
}
