package it.si2001.bankproject.service.impl;

import it.si2001.bankproject.domain.Balance;
import it.si2001.bankproject.domain.dto.Response;
import it.si2001.bankproject.service.BalanceService;
import it.si2001.bankproject.util.ApiUtilities;
import it.si2001.bankproject.util.ErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

/**
 * Service Implementation for managing {@link Balance}.
 */
@Service
public class BalanceServiceImpl implements BalanceService {

    private final Logger log = LoggerFactory.getLogger(BalanceServiceImpl.class);

    @Value("${external.api.baseurl}")
    private String BASE_URL;

    @Override
    public Response getAccountBalance(Long id){
        try{
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<Response> response = restTemplate.exchange(
                    BASE_URL +"/api/gbs/banking/v4.0/accounts/"+ id.toString() + "/balance",
                    HttpMethod.GET,
                    ApiUtilities.getApiCallHeaders(null),
                    Response.class);

            Response r = response.getBody();
            return r;

        }catch(HttpServerErrorException errorException){
            return ErrorHandler.errorResponse(errorException);
        }
    }
}
