package it.si2001.bankproject.service.impl;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import it.si2001.bankproject.domain.Transaction;
import it.si2001.bankproject.domain.dto.ResponseList;
import it.si2001.bankproject.repository.TransactionRepository;
import it.si2001.bankproject.service.TransactionService;
import it.si2001.bankproject.util.ApiUtilities;
import it.si2001.bankproject.util.ErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Service Implementation for managing {@link Transaction}.
 */
@Service
public class TransactionServiceImpl implements TransactionService {

    private final Logger log = LoggerFactory.getLogger(TransactionServiceImpl.class);

    @Value("${external.api.baseurl}")
    private String BASE_URL;

    private final TransactionRepository transactionRepository;

    public TransactionServiceImpl(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }


    @Override
    public ResponseList findBetweenDates(Long id, Date from, Date to) {
        try{
            String pattern = "yyyy-MM-dd";
            SimpleDateFormat df = new SimpleDateFormat(pattern);
            String urlTemplate = UriComponentsBuilder.fromHttpUrl(BASE_URL +"/api/gbs/banking/v4.0/accounts/"+ id.toString() + "/transactions")
                    .queryParam("fromAccountingDate", df.format(from))
                    .queryParam("toAccountingDate", df.format(to))
                    .encode()
                    .toUriString();
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<ResponseList> response = restTemplate.exchange(
                    urlTemplate,
                    HttpMethod.GET,
                    ApiUtilities.getApiCallHeaders(null),
                    ResponseList.class);

            ResponseList r = response.getBody();

            Gson gson = new Gson();
            Type listType = new TypeToken<ArrayList<Transaction>>(){}.getType();
            String json = gson.toJson(r.getPayload().getList());
            List<Transaction> l = gson.fromJson(json, listType);
            for (Transaction t : l) {
                if(!transactionRepository.existsById(t.getTransactionId()))
                    transactionRepository.save(t);
            }
            return r;

        }catch(HttpServerErrorException errorException){
            return ErrorHandler.errorResponseList(errorException);
        }
    }
}
