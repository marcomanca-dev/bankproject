package it.si2001.bankproject.service;

import it.si2001.bankproject.domain.Payment;
import it.si2001.bankproject.domain.Transaction;
import it.si2001.bankproject.domain.dto.Response;

import java.util.Date;
import java.util.List;

/**
 * Service Interface for managing {@link Transaction}.
 */
public interface PaymentService {

    /**
     * Execute Payment.
     *
     * @param id the id of the account.
     * @param payment to execute
     * @return the entity.
     */
    Response executePayment(Long id, Payment payment);
}
