package it.si2001.bankproject.service;

import it.si2001.bankproject.domain.Transaction;
import it.si2001.bankproject.domain.dto.ResponseList;

import java.util.Date;

/**
 * Service Interface for managing {@link Transaction}.
 */
public interface TransactionService {

    /**
     * Get the list of transactions by account id and between dates.
     *
     * @param id the id of the account.
     * @param from date from search.
     * @param to date to search.
     * @return the entity.
     */
    ResponseList findBetweenDates(Long id, Date from, Date to);
}
