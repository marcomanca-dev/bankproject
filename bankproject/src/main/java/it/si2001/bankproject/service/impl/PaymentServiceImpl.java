package it.si2001.bankproject.service.impl;

import com.google.gson.Gson;
import it.si2001.bankproject.domain.Payment;
import it.si2001.bankproject.domain.Transaction;
import it.si2001.bankproject.domain.dto.Response;
import it.si2001.bankproject.service.PaymentService;
import it.si2001.bankproject.util.ApiUtilities;
import it.si2001.bankproject.util.ErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;


/**
 * Service Implementation for managing {@link Transaction}.
 */
@Service
public class PaymentServiceImpl implements PaymentService {

    private final Logger log = LoggerFactory.getLogger(PaymentServiceImpl.class);

    @Value("${external.api.baseurl}")
    private String BASE_URL;


    @Override
    public Response executePayment(Long id, Payment payment){
        try{
            RestTemplate restTemplate = new RestTemplate();
            Gson gson = new Gson();
            String uri = BASE_URL +"/api/gbs/banking/v4.0/accounts/"+ id.toString() + "/payments/money-transfers";
            ResponseEntity<Response> response =
                    restTemplate.exchange(uri,
                    HttpMethod.POST,
                    ApiUtilities.getApiCallHeaders(gson.toJson(payment)),
                    Response.class);

            Response r = response.getBody();
            return r;

        }catch(HttpServerErrorException errorException){
            return ErrorHandler.errorResponse(errorException);
        }
    }
}
