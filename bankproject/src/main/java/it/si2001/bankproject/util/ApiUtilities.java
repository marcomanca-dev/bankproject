package it.si2001.bankproject.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

public class ApiUtilities {

    @Value("${external.api.authschema}")
    private static final String AUTH_SCHEMA = "S2S";

    @Value("${external.api.apikey}")
    private static final String API_KEY = "FXOVVXXHVCPVPBZXIJOBGUGSKHDNFRRQJP";

    @Value("${external.api.timezone}")
    private static final String TIME_ZONE = "Europe/Rome";


    public static HttpEntity<String> getApiCallHeaders(String body){
        HttpEntity<String> entity;
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.set("Auth-Schema", AUTH_SCHEMA);
        headers.set("apikey", API_KEY);
        headers.set("X-Time-Zone", TIME_ZONE);
        if(null != body && !StringUtils.isEmpty(body))
            entity = new HttpEntity<>(body, headers);
        else
            entity = new HttpEntity<>("body", headers);

        return entity;
    }

}
