package it.si2001.bankproject.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import it.si2001.bankproject.domain.dto.Response;
import it.si2001.bankproject.domain.dto.ResponseList;
import org.apache.tomcat.util.json.JSONParser;
import org.json.JSONObject;
import org.springframework.boot.json.JsonParser;
import org.springframework.web.client.HttpServerErrorException;

import java.util.ArrayList;
import java.util.List;

import it.si2001.bankproject.domain.dto.Error;

public class ErrorHandler {

    public static Response errorResponse(HttpServerErrorException errorException) {
        Response r = new Response();
        r.setStatus("KO");
        Gson gson = new Gson();
        String error = errorException.getResponseBodyAsString().replace("\r\n", "");
        r = gson.fromJson(error, Response.class);
        return r;
    }

    public static ResponseList errorResponseList(HttpServerErrorException errorException) {
        ResponseList r = new ResponseList();
        r.setStatus("KO");
        Gson gson = new Gson();
        String error = errorException.getResponseBodyAsString().replace("\r\n", "");
        r = gson.fromJson(error, ResponseList.class);
        return r;
    }
}
