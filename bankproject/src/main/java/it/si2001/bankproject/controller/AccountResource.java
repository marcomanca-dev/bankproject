package it.si2001.bankproject.controller;

import it.si2001.bankproject.domain.dto.Response;
import it.si2001.bankproject.domain.dto.ResponseList;
import it.si2001.bankproject.service.BalanceService;
import it.si2001.bankproject.service.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Date;

/**
 * REST controller for managing Account operations.
 */
@RestController
@RequestMapping("/api")
public class AccountResource {

    private final Logger log = LoggerFactory.getLogger(AccountResource.class);

    private static final String ENTITY_NAME = "account";

    @Value("${client.app.name}")
    private String applicationName;

    private final BalanceService balanceService;
    private final TransactionService transactionService;

    public AccountResource(BalanceService balanceService, TransactionService transactionService){
        this.balanceService = balanceService;
        this.transactionService = transactionService;
    }

    /**
     * {@code GET  /reports/:accountId/balance} : get the "accountId" account.
     *
     * @param accountId the id of the account balance to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the balance, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/accounts/{accountId}/balance")
    public ResponseEntity<Response> getAccountBalance(@PathVariable Long accountId){
        log.debug("REST request to get Account Balance : {}", accountId);
        Response r = balanceService.getAccountBalance(accountId);
        if(r.getStatus().equalsIgnoreCase(HttpStatus.OK.getReasonPhrase()))
            return ResponseEntity.ok().body(r);
        else
            return ResponseEntity.badRequest().body(r);
    }

    /**
     * {@code GET  /accounts/{accountId}/transactions} : get all the transactions by accountId and between dates.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of transactions in body.
     */
    @GetMapping("/accounts/{accountId}/transactions")
    public ResponseEntity<ResponseList> getAllTransactionsBetweenDates(@PathVariable Long accountId,
                                                            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") Date fromAccountingDate,
                                                            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") Date toAccountingDate){
        log.debug("REST request to get all transaction by accountId and dates");
        ResponseList r = transactionService.findBetweenDates(accountId, fromAccountingDate, toAccountingDate);
        if(r.getStatus().equalsIgnoreCase(HttpStatus.OK.getReasonPhrase()))
            return ResponseEntity.ok().body(r);
        else
            return ResponseEntity.badRequest().body(r);
    }
}
