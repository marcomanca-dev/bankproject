package it.si2001.bankproject.controller;

import it.si2001.bankproject.domain.Payment;
import it.si2001.bankproject.domain.dto.Response;
import it.si2001.bankproject.service.PaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * REST controller for managing Account operations.
 */
@RestController
@RequestMapping("/api")
public class PaymentResource {

    private final Logger log = LoggerFactory.getLogger(PaymentResource.class);

    private static final String ENTITY_NAME = "payment";

    @Value("${client.app.name}")
    private String applicationName;

    private final PaymentService paymentService;

    public PaymentResource(PaymentService paymentService){
        this.paymentService = paymentService;
    }

    /**
     * {@code GET  /reports/:accountId/balance} : get the "accountId" account.
     *
     * @param accountId the id of the account balance to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the balance, or with status {@code 404 (Not Found)}.
     */
    @PostMapping("/accounts/{accountId}/payments/money-transfers")
    public ResponseEntity<Response> executePayment(@Validated @PathVariable Long accountId, @RequestBody Payment payment){
        log.debug("REST request to execute a payment : {}", accountId);
        Response r = paymentService.executePayment(accountId, payment);
        if(r.getStatus().equalsIgnoreCase(HttpStatus.OK.getReasonPhrase()))
            return ResponseEntity.ok().body(r);
        else
            return ResponseEntity.badRequest().body(r);
    }

}
