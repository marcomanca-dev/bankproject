package it.si2001.bankproject.domain;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import java.io.Serializable;
import java.util.Date;

@Getter @Setter @NoArgsConstructor
public class Balance implements Serializable {

    @NonNull
    private Date date;

    @NotNull
    private Double balance;

    @NotNull
    private Double availableBalance;

    @NotNull
    private String currency;

    public Balance(@NonNull Date date, Double balance, Double availableBalance, String currency) {
        this.date = date;
        this.balance = balance;
        this.availableBalance = availableBalance;
        this.currency = currency;
    }
}
