package it.si2001.bankproject.domain;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Getter@Setter
public class Payment {
    @NonNull
    private Creditor creditor;
    private String executionDate;
    private String uri;
    @NonNull
    private String description;
    @NonNull
    private Double amount;
    @NonNull
    private String currency;
    private boolean isUrgent;
    private boolean isInstant;
    private String feeType;
    private String feeAccountId;
    private TaxRelief taxRelief;


    @Getter @Setter
    public class Creditor {

        @NonNull
        private String name;
        @NonNull
        private Account account;
        private Address address;

        @Getter @Setter
        public class Account{
            @NonNull
            private String accountCode;
            private String bicCode;
        }

        @Getter @Setter
        public class Address{
            private String address;
            private String city;
            private String countryCode;
        }


    }

    @Getter @Setter
    public class TaxRelief {

        private String taxReliefId;
        @NonNull
        private boolean isCondoUpgrade;
        @NonNull
        private String creditorFiscalCode;
        @NonNull
        private String beneficiaryType;

        private NaturalPersonBeneficiary naturalPersonBeneficiary;
        private LegalPersonBeneficiary legalPersonBeneficiary;

        @Getter @Setter
        public class NaturalPersonBeneficiary{
            @NonNull
            private String fiscalCode1;
            private String fiscalCode2;
            private String fiscalCode3;
            private String fiscalCode4;
            private String fiscalCode5;
        }

        @Getter @Setter
        public class LegalPersonBeneficiary{
            @NonNull
            private String fiscalCode;
            private String legalRepresentativeFiscalCode;
        }


    }
}
