package it.si2001.bankproject.domain.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter @Setter
public class Response {

    private String status;
    private List<Error> errors = new ArrayList<>();
    private Object payload;

}
