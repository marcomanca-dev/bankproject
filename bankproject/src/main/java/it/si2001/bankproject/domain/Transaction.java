package it.si2001.bankproject.domain;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Entity
@Getter @Setter @NoArgsConstructor
public class Transaction implements Serializable {


    private @Id Long transactionId;

    @NonNull
    private Long operationId;

    @NotNull
    private Date accountingDate;

    @NotNull
    private Date valueDate;


    @NotNull
    private Double amount;

    @NotNull
    private String currency;

    @NotNull
    private String description;

    public Transaction(@NonNull Long operationId, Date accountingDate, Date valueDate, Double amount, String currency, String description) {
        this.operationId = operationId;
        this.accountingDate = accountingDate;
        this.valueDate = valueDate;
        this.amount = amount;
        this.currency = currency;
        this.description = description;
    }
}
