package it.si2001.bankproject.domain.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Error{
    private String code;
    private String description;
    private String params;
}