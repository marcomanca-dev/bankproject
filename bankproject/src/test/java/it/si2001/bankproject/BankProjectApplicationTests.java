package it.si2001.bankproject;

import it.si2001.bankproject.controller.AccountResource;
import it.si2001.bankproject.controller.PaymentResource;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class BankProjectApplicationTests {

	@Autowired
	private AccountResource accountResource;

	@Autowired
	private PaymentResource paymentResource;

	@Test
	void contextLoads() {
		assertThat(accountResource).isNotNull();
		assertThat(paymentResource).isNotNull();
	}

}
