package it.si2001.bankproject.controller;

import com.google.gson.Gson;
import it.si2001.bankproject.domain.Payment;
import it.si2001.bankproject.domain.dto.Response;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PaymentResourceTest {

    private final static Long ACCOUNTID = 14537780L;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void executePaymentTest(){
        String baseUrl = "http://localhost:" + port + "/api/accounts/" + ACCOUNTID + "/payments/money-transfers";
        String json = "{\"creditor\":{\"name\":\"John Doe\",\"account\":{\"accountCode\":\"IT23A0336844430152923804660\",\"bicCode\":\"SELBIT2BXXX\"},\"address\":{}},\"executionDate\":\"2019-04-01\",\"uri\":\"REMITTANCE_INFORMATION\",\"description\":\"Payment invoice 75/2017\",\"amount\":800.0,\"currency\":\"EUR\",\"isUrgent\":false,\"isInstant\":false,\"feeType\":\"SHA\",\"feeAccountId\":\"45685475\",\"taxRelief\":{\"taxReliefId\":\"L449\",\"isCondoUpgrade\":false,\"creditorFiscalCode\":\"56258745832\",\"beneficiaryType\":\"NATURAL_PERSON\",\"naturalPersonBeneficiary\":{\"fiscalCode1\":\"MRLFNC81L04A859L\"},\"legalPersonBeneficiary\":{}}}";
        Gson gson = new Gson();
        ResponseEntity<Response> response = this.restTemplate.postForEntity(baseUrl, gson.fromJson(json, Payment.class), Response.class);

        Assert.isTrue(response.getStatusCode().equals(HttpStatus.BAD_REQUEST), "Error on executePaymentTest, the status code is " + response.getStatusCode() + " not " + HttpStatus.OK);
        Assert.isTrue(response.getBody().getStatus().equals("400"), "Error on executePaymentTest, the internal status code is " + response.getBody().getStatus() + " not " + HttpStatus.OK.value());
    }


}
