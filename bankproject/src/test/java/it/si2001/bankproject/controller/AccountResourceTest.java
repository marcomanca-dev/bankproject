package it.si2001.bankproject.controller;

import it.si2001.bankproject.domain.dto.Response;
import it.si2001.bankproject.domain.dto.ResponseList;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import java.text.SimpleDateFormat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AccountResourceTest {

    private final static Long ACCOUNTID = 14537780L;


    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;


    @Test
    public void getAccountBalanceTest(){
        String baseUrl = "http://localhost:" + port + "/api/accounts/" + ACCOUNTID + "/";
        ResponseEntity<Response> response = this.restTemplate.getForEntity(baseUrl + "balance", Response.class);

        Assert.isTrue(response.getStatusCode().equals(HttpStatus.OK), "Error on getAccountBalanceTest, the status code is " + response.getStatusCode() + " not " + HttpStatus.OK);
        Assert.isTrue(response.getBody().getPayload().toString().contains("balance"), "Error on getAccountBalanceTest, the response not contain assertion parameter");
    }

    @Test
    public void getAllTransactionsBetweenDatesTest(){
        String baseUrl = "http://localhost:" + port + "/api/accounts/" + ACCOUNTID + "/";

        String pattern = "yyyy-MM-dd";
        SimpleDateFormat df = new SimpleDateFormat(pattern);
        String urlTemplate = UriComponentsBuilder.fromHttpUrl(baseUrl + "transactions")
                .queryParam("fromAccountingDate", "2022-01-01")
                .queryParam("toAccountingDate", "2022-01-27")
                .encode()
                .toUriString();
        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<ResponseList> response = this.restTemplate.getForEntity(urlTemplate, ResponseList.class);

        Assert.isTrue(response.getStatusCode().equals(HttpStatus.OK), "Error on getAllTransactionsBetweenDatesTest, the status code is " + response.getStatusCode() + " not " + HttpStatus.OK);
        Assert.isTrue(ArrayUtils.isNotEmpty(response.getBody().getPayload().getList()), "Error on getAllTransactionsBetweenDatesTest, the response contain a empty list, but in the selected dates there should be transactions");
    }
}
